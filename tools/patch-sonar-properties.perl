#!/usr/bin/env perl -w

s{(?:^#\s*)?(sonar.links.homepage=).*}{$1$ENV{CI_PROJECT_URL}}g;
s{(?:^#\s*)?(sonar.links.scm=).*}{$1$ENV{CI_PROJECT_URL}.git}g;
s{(?:^#\s*)?(sonar.projectKey=).*}{$1$Key}g;
s{(?:^#\s*)?(sonar.c.includeDirectories=).*}{$1$Include}g;

BEGIN {
  # Project key is gitlab project path, plus commit ref slug
  ($Key = ($ENV{CI_PROJECT_PATH} ||= "sed-bso:heat")) =~ s{/}{:}g;
  $Key .= ":gitlab";

  # Header file directories are the first words of lines starting with whitespace
  # in the result of gcc -E, plus MPI and local directories
  chomp(@F = qx{echo | gcc -E -Wp,-v - 2>&1});
  $Include = join(",",
                  (map { $_=~s{\s*(\S+).*}{$1}; $_ } grep { /^ / } @F),
                  qw(/usr/lib/x86_64-linux-gnu/openmpi/include include .));
}
