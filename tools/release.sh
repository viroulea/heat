#!/bin/sh
set -x
mkdir -p build && cd build && cmake ..
filefull=`make package | grep .deb | awk '{print $4 }'`
file=$(basename ${filefull})
version=`grep Version _CPack_Packages/Linux/DEB/heat-*-Linux/control | awk '{print $2}'`
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $file "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/heat/$version/$file"
