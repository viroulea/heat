HEAT
====================

[![pipeline status](https://gitlab.inria.fr/sed-bso/heat/badges/master/pipeline.svg)](https://gitlab.inria.fr/sed-bso/heat/commits/master)
[![coverage report](https://gitlab.inria.fr/sed-bso/heat/badges/master/coverage.svg)](https://gitlab.inria.fr/sed-bso/heat/commits/master)
[![sonarqube status](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=sed-bso%3Aheat%3Agitlab&metric=alert_status&token=sqb_154b0ee8376b1e903d0748762f9694f0a185f7ec)](https://sonarqube.inria.fr/sonarqube/dashboard?id=sed-bso%3Aheat%3Agitlab)

Mathematical problem
---------------------

This C program aims at solving the following **heat propagation** equation

```math
\frac{\partial u(x,t) }{\partial t} - \Delta u(x,t) = 0 \qquad \forall  t \in [0,T] \, , \forall x \in [0,1]^2
```
```math
u(x,t) = 1 \, \qquad \forall  t \in [0,T] \, , \forall x \in \partial [0,1]^2.
```

Project
---------------------

This program serves as a toy code.
Several software engineering techniques are used:

* CMake build system with CTest
* Doxygen documentation
* A pipeline to test the code, either gitlab-ci (.gitlab-ci.yml) or Jenkins (Jenkinsfile) can be used
* Org-mode script for the code analysis and the integration into a SonarQube instance

Formation
---------------------

* [Gitlab-CI](https://sed-bso.gitlabpages.inria.fr/gitlab-ci/)
* [SonarQube](https://sed-bso.gitlabpages.inria.fr/sonarqube/)